#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794"
<define-tag pagetitle>DebConf19 slutter i Curitiba, og datoer for DebConf20 annonceret</define-tag>

<define-tag release_date>2019-07-27</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>I dag, lørdag den 27. juli 2019, sluttede den årlige Debian Developers and 
Contributors Conference.  Med over 380 deltagere fra 50 forskellige lande, og 
flere end 145 foredrag, diskussionsseancer og diskussionsseancer (BoF'er), 
workshops og andre aktiviteter, blev <a href="https://debconf19.debconf.org">\
DebConf19</a> en stor succes.</p>

<p>Forud for konferencen fandt den årlige DebCamp sted fra den 14. til 19 juli, 
med fokus på individuelt arbejde og teamsprinter til personligt tilstedeværende 
samarbejde på at udvikle Debian, samt en tredages pakningsworkstop, hvor nye 
bidragydere kunne begynde på Debian-pakning.</p>

<p><a href="https://debconf19.debconf.org/news/2019-07-20-open-day/">Open 
Day</a> blev afholdt den 20. juli, med flere end 250 deltagere, som overværede 
præsentationer og workshops af interesse for et bredere publikum, et jobmarked 
med stande fra flere af DebConf19's sponsorer og en 
Debian-installeringsfest.</p>

<p>Den egentlige Debian Developers Conference begyndte søndag den 21. juli 2019. 
Sammen med plenarer, så som den traditionelle <q>Bits fra DPL'en</q>, lynhurtige 
foredrag, livedemonstrationer og annoncering af næste års DebConf
(<a href="https://wiki.debian.org/DebConf/20">DebConf20</a> i Haifa, Israel), 
samt nogle seancer i forbindelse med den nylige udgivelse af Debian 10 buster, 
og noget af dens nye funktionalitet, foruden nyhedsopdatering fra flere 
projekter og interne Debian-hold, diskussionsseancer (BoF'er) fra sprog-, 
tilpasnings-, infrastruktur- og fællesskabsholdene, med mange andre aktiviteter 
med relation til Debian og fri software.</p>

<p><a href="https://debconf19.debconf.org/schedule/">Programmet</a> blev 
opdateret daligt med planlagte og ad-hoc-aktiviteter, planlagt af deltagerne i 
løbet af hele konferencen.</p>

<p>For dem, der ikke var i stand til at deltage, blev de fleste foredrag optaget 
og livestreamet, og videoer blev gjort tilgængelige på 
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/">\
Debians arkivwebsted for møder</a>.  I mange seancer var der også 
fjerndeltagelse over IRC eller et samarbejstekstdokument.</p>

<p>Webstedet <a href="https://debconf19.debconf.org/">DebConf19</a> vil af 
arkiveringshensyn for blive aktivt, og vil fortsat indeholde links til 
præsentationer og videooptagelser af foredrag og begivenheder.</p>

<p>Næste år finder <a href="https://wiki.debian.org/DebConf/20">DebConf20</a> 
sted i Haifa, Israel, fra den 23. til 29. august 2020.  Traditionen tro holder 
de lokale organisatorer igen DebCamp (16.-22. august), med færligt fokus på 
individuelt og teamarbejde med henblik på forbedring af distributionen.</p>

<p>DebConf har forpligtet sig til at være et sikkert og imødekommende miljø for 
alle deltagere.  Under konferencen er flere hold (forkontor, velkomst og 
antichikane) til stede for at hjælpe med at både tilstedeværende og fjerne 
deltagere får den bedste konferenceoplevelse, og finder løsninger på alle 
problemer, der måtte opstå.  Se 
<a href="https://debconf19.debconf.org/about/coc/">websiden om de etiske 
retningslinjer på DebConf19's websted</a>, for flere oplysninger herom.</p>

<p>Debian takker de talrige 
<a href="https://debconf19.debconf.org/sponsors/">sponsorer</a> for støtten 
til DebConf19, i særdeleshed vores platinsponsorer 
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a> og 
<a href="https://www.lenovo.com">Lenovo</a>.</p>


<h2>Om Debian</h2>

<p>Debian-projektet blev grundlagt i 1993 af Ian Murdock, som et helt frit 
fællesskabsprojekt.  Siden den gang, er projektet vokset til at være et af de 
største og mest indflydelsesrige open source-projekter.  Tusindvis af
frivillige fra hele verden samarbejder om at fremstille og vedligeholde 
Debian-software.  Med oversættelser til 70 sprog, og med understøttelse af et 
enormt antal computertyper, kalder Debian sig det <q>universelle 
styresystem</q>.</p>


<h2>Om DebConf</h2>

<p>DebConf er Debian-projektets udviklerkonference.  Ud over et fuldt program 
med tekniske, sociale og retningslinjeforedrag, gør DebConf det også muligt 
at udviklere, bidragydere og andre interesserede kan mødes personligt og 
arbejde tættere sammen.  Konferencen har fundet sted siden år 2000 i så 
forskellige steder som Skotland, Argentina og Bosien-Hercegovina.  Flere 
oplysninger er tilgængelige på <a href="https://debconf.org/">\
DebConf-webstedet</a>.</p>


<h2>Om Infomaniak</h2>

<p><a href="https://www.infomaniak.com">Infomaniak</a> er Schweiz' største 
webhostingvirksomhed, der desuden tilbyder backup- og storagetjenester, 
løsninger til begivenhedsorganisatorer, livestreamning og video on 
demand-tjenster.  De ejer selv deres datacentre og alle elementer, der er 
afgørende for at de tjenester og produkter, som virksomheden tilbyder, 
fungerer (både software og hardware).</p>


<h2>Om Google</h2>

<p><a href="https://google.com/">Google</a> er en af de største 
teknologivirksomheder i verden, og tilbyder et bredt udvalg af 
internetrelaterede tjenester og produkter, så som onlinereklameteknologier, 
søgning, cloudcomputing, software og hardware.</p>

<p>Google har støttet Debian ved at sponsere DebConf i mere end ti år, og er 
desuden en Debian-partner, som sponserer dele af 
<a href="https://salsa.debian.org">Salsa</a>s continuous 
integration-infrastruktur på Googles cloudplatform.</p>


<h2>Om Lenovo</h2>

<p>Som en global teknologileder med fremstilling af en bred portefølje af 
forbundne produkter, herunder smartphones, tablets, PC'er og workstations, 
foruden AR/VR-enheder, smarthome/-kontor og datacenterløsninger, forstår 
<a href="https://www.lenovo.com">Lenovo</a> hvor afgørende åbne systemer og 
platforme er for den forbundne verden.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg DebConf19's websider på
<a href="https://debconf19.debconf.org/">https://debconf19.debconf.org/</a>
eller send en engelsksproget mail til &lt;press@debian.org&gt;.</p>
