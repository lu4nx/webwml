<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Zhuowei Zhang discovered a bug in the EAP authentication client code of
strongSwan, an IKE/IPsec suite, that may allow to bypass the client and in some
scenarios even the server authentication, or could lead to a denial-of-service
attack.</p>

<p>When using EAP authentication (RFC 3748), the successful completion of the
authentication is indicated by an EAP-Success message sent by the server to the
client. strongSwan's EAP client code handled early EAP-Success messages
incorrectly, either crashing the IKE daemon or concluding the EAP method
prematurely.</p>

<p>End result depend on the used configuration, more details can be found in
upstream advisory at
<a href="https://www.strongswan.org/blog/2022/01/24/strongswan-vulnerability-(cve-2021-45079).html">\
https://www.strongswan.org/blog/2022/01/24/strongswan-vulnerability-(cve-2021-45079).html</a></p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 5.7.2-1+deb10u2.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 5.9.1-1+deb11u2.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>For the detailed security status of strongswan please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5056.data"
# $Id: $
