<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been found in Asterisk, an Open Source
Private Branch Exchange. Buffer overflows and other programming errors could be
exploited for information disclosure or the execution of arbitrary code.</p>

<p>Special care should be taken when upgrading to this new upstream release.
Some configuration files and options have changed in order to remedy
certain security vulnerabilities. Most notably the pjsip TLS listener only
accepts TLSv1.3 connections in the default configuration now. This can be
reverted by adding method=tlsv1_2 to the transport in pjsip.conf. See also
<a href="https://issues.asterisk.org/jira/browse/ASTERISK-29017">\
https://issues.asterisk.org/jira/browse/ASTERISK-29017</a>.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1:16.28.0~dfsg-0+deb11u1.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>For the detailed security status of asterisk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/asterisk">\
https://security-tracker.debian.org/tracker/asterisk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5285.data"
# $Id: $
