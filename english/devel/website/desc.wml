#use wml::debian::template title="How www.debian.org is made" MAINPAGE="true"
#use wml::debian::toc

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">Look &amp; Feel</a></li>
<li><a href="#sources">Sources</a></li>
<li><a href="#scripts">Scripts</a></li>
<li><a href="#generate">Generating the Website</a></li>
<li><a href="#help">How to help</a></li>
<li><a href="#faq">How not to help... (FAQ)</a></li>
</ul>

<h2><a id="look">Look &amp; Feel</a></h2>

<p>The Debian website is a collection of directories 
and files located in <code>/org/www.debian.org/www</code> 
on <em>www-master.debian.org</em>. Most pages 
are static HTML files. They don't contain dynamic elements 
like CGI or PHP scripts, because the website is mirrored.
</p>

<p>
The Debian website uses the Website Meta Language 
(<a href="https://packages.debian.org/unstable/web/wml">WML</a>) to generate the HTML pages, 
including headers and footers, headings, table of contents, etc. 
Although a <code>.wml</code> file might look like HTML at first glance, HTML is only one of the types 
of extra information that can be used in WML. You can also include Perl code into a page and therefore 
do almost anything.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Our web pages currently comply to the <a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a> standard.</p>
</aside>

<p>
After WML is finished running its various filters over a file, 
the final product is true HTML. Please note that although WML checks
(and sometimes automagically corrects) the basic validity of your HTML code, 
you should install a tool like 
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a> 
and/or 
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a> 
as syntax and minimal style checker.
</p>

<p>
Anyone who is regularly contributing to the Debian website should install
WML to test the code and make sure the resulting HTML pages look right. If
you are running Debian, simply install the <code>wml</code> package. 
For more information, have a look at the <a href="using_wml">using WML</a> page.
</p>

<h2><a id="sources">Sources</a></h2>

<p>
We use Git to store the Debian website's sources. The version control 
system allows us to keep track of all changes and we can see who committed 
what and when, even why. Git offers a safe way to control the 
concurrent editing of source files by multiple authors — a 
crucial task for the Debian web team, because it has a lot of members.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">Read more about Git</a></button></p>

<p>
Here is some background information on how the sources are structured:
</p>

<ul>
  <li>The topmost directory in the Git repository (<code>webwml</code>) contains 
directories named after the respective pages' language, 
two Makefile and several scripts. Directory names for translated pages 
should be in English and use lowercase letters, for example <code>german</code> and not <code>Deutsch</code>.</li>

  <li>The file <code>Makefile.common</code> is especially important, 
as it contains some common rules which are applied by including this file 
in the other Makefiles.</li>

  <li>All the subdirectories for the different languages also contain Makefiles, various <code>.wml</code> source files, 
and additional subdirectories. All file and directory names follow a certain pattern, 
so that every link works for all the translated pages. 
Some directories also contain a <code>.wmlrc</code> configuration file with 
additional commands and preferences for WML.</li>

  <li>The directory <code>webwml/english/template</code> contains special WML files which 
work as templates. They can be referenced from all other 
files with the <code>#use</code> command.</li>
</ul>

<p>
Please note: to ensure that changes in the templates get propagated to the files 
which include them, the files have Makefile dependencies on them. 
A vast majority of the files use the <code>template</code> template, 
the generic dependency, so they contain the following line at the top:
</p>

<p>
<code>#use wml::debian::template</code>
</p>

<p>
There are exceptions to this rule, of course.
</p>

<h2><a id="scripts">Scripts</a></h2>

<p>
The scripts are mostly written in shell or Perl. Some 
of them work standalone, and some of them are integrated into 
WML source files.
</p>

<ul>
  <li><a href="https://salsa.debian.org/webmaster-team/cron.git">webmaster-team/cron</a>: 
This Git repositors contains all the scripts used to update the Debian web site, 
i.e. the sources for the <code>www-master</code> rebuild scripts.</li>
  <li><a href="https://salsa.debian.org/webmaster-team/packages">webmaster-team/packages</a>: 
This Git repository contains the sources for the <code>packages.debian.org</code> rebuild scripts.</li>
</ul>

<h2><a id="generate">Generating the Website</a></h2>

<p>
WML, templates, and shell or Perl scripts are all the ingredients you need to generate the Debian website:
</p>

<ul>
  <li>The majority is generated using WML (from the <a href="$(DEVEL)/website/using_git">Git repository</a>).</li>
  <li>The documentation is generated with either DocBook XML (<a href="$(DOC)/vcs"><q>ddp</q> Git repository</a>) 
or with <a href="#scripts">cron scripts</a> from the corresponding Debian packages.</li> 
  <li>Some parts of the website are generated with scripts using other sources, 
for example, the mailing list (un)subscription pages.</li>
</ul>

<p>
An automatic update (from the Git repository and other sources to the webtree) is being run six times a day. 
Apart from that, we regularly run the following checks on the whole website:
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL check</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>
The current build logs for the website 
can be found at <url "https://www-master.debian.org/build-logs/">.
</p>

<p>If you'd like to contribute to the site, do <strong>not</strong> simply
edit files in the <code>www/</code> directory or add new items. Instead,
please contact the <a href="mailto:webmaster@debian.org">webmasters</a> first.
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span> On a more technical note: 
all files and directories are owned by the <code>debwww</code> group which has write permission. 
That way, the web team can modify content in the web directory. 
The <code>2775</code> mode on directories means that any files created here will inherit the group (<code>debwww</code>). 
Members of the group are expected to set <code>umask 002</code> so that files are created with group write permissions.</p>
</aside>

<h2><a id="help">How to help</a></h2>

<p>
We encourage anyone to help with the Debian website. If you have 
valuable information related to Debian which you think is missing, please 
<a href="mailto:debian-www@lists.debian.org">get in touch</a> — we'll see that it gets included. 
Also, please have a look at the above mentioned 
<a href="https://www-master.debian.org/build-logs/">build logs</a> and see if you have 
suggestions for fixing a problem.
</p>

<p>
We're also looking for people who can help with the design 
(graphics, layouts, etc.). If you're a fluent English speaker, 
please consider proof-reading our pages and 
<a href="mailto:debian-www@lists.debian.org">report</a> errors. 
If you speak another language, you may want to help to translate 
existing pages or help with fixing bugs in already translated pages. 
In both cases, please have a look at the list of 
<a href="translation_coordinators">translation coordinators</a> and 
get in touch with the responsible person. For more information, 
please have a look at our <a href="translating">page for translators</a>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">Read our TODO list</a></button></p>


<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">How not to help... (FAQ)</a></h2>

<p>
<strong>[Q] I want to include <em>this fancy feature</em> in the Debian website, may I?</strong>
</p>

<p>
[A] No. We want www.debian.org to be as accessible as possible, so
</p>

<ul>
    <li>no browser-specific "extensions",
    <li>no relying on images only. Images may be used to clarify, but the
        information on www.debian.org must remain accessible via a text-only
        web browser, like lynx.
</ul>

<p>
<strong>[Q] I have this nice idea I want to submit. Can you enable <em>foo</em> or <em>bar</em> in 
www.debian.org's HTTP server, please?</strong>
</p>

<p>
[A] No. We want to make life easy for administrators 
to mirror www.debian.org, so no special HTTPD features, please. 
No, not even SSI (Server Side Includes). An exception has been made 
for content negotiation, because that's the only robust way to serve multiple languages.
</p>
