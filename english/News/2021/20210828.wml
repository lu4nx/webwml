<define-tag pagetitle>DebConf21 online closes</define-tag>

<define-tag release_date>2021-08-28</define-tag>
#use wml::debian::news

<p>
On Saturday 28 August 2021, the annual Debian Developers
and Contributors Conference came to a close.
</p>

<p>
DebConf21 has been held online for the second time, due to the coronavirus 
(COVID-19) disease pandemic. 
</p>

<p>
All of the sessions have been streamed, with a variety of ways of participating:
via IRC messaging, online collaborative text documents,
and video conferencing meeting rooms.
</p>

<p>
With 740 registered attendees from more than 15 different countries and a
total of over 70 event talks, discussion sessions,
Birds of a Feather (BoF) gatherings and other activities,
<a href="https://debconf21.debconf.org">DebConf21</a> was a large success.
</p>

<p>
The setup made for former online events involving Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad, a web-based
frontend for voctomix has been improved and used for DebConf21 successfully.
All components of the video infrastructure are free software, and configured through the 
Video Team's public <a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a> repository.
</p>

<p>
The DebConf21 <a href="https://debconf21.debconf.org/schedule/">schedule</a> included
a wide variety of events, grouped in several tracks:
</p>
<ul>
<li>Introduction to Free Software &amp; Debian,</li>
<li>Packaging, policy, and Debian infrastructure,</li>
<li>Systems administration, automation and orchestration,</li>
<li>Cloud and containers,</li>
<li>Security,</li>
<li>Community, diversity, local outreach and social context,</li>
<li>Internationalization, Localization and Accessibility,</li>
<li>Embedded &amp; Kernel,</li>
<li>Debian Blends and Debian derived distributions,</li>
<li>Debian in Arts &amp; Science</li>
<li>and other.</li>
</ul>
<p>
The talks have been streamed using two rooms, and several of these activities
have been held in different languages: Telugu, Portuguese, Malayalam, Kannada,
Hindi, Marathi and English, allowing a more diverse audience to enjoy and participate.
</p>

<p>
Between talks, the video stream has been showing the usual sponsors on the loop, but also
some additional clips including photos from previous DebConfs, fun facts about Debian
and short shout-out videos sent by attendees to communicate with their Debian friends. 
</p>

<p>The Debian publicity team did the usual «live coverage» to encourage participation
with micronews announcing the different events. The DebConf team also provided several 
<a href="https://debconf21.debconf.org/schedule/mobile/">mobile options to follow
the schedule</a>.
</p>

<p>
For those who were not able to participate, most of the talks and sessions are already
available through the
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/">Debian meetings archive website</a>,
and the remaining ones will appear in the following days.
</p>

<p>
The <a href="https://debconf21.debconf.org/">DebConf21</a> website
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.
</p>

<p>
Next year, <a href="https://wiki.debian.org/DebConf/22">DebConf22</a> is planned to be held
in Prizren, Kosovo, in July 2022.
</p>

<p>
DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Community team)
have been available to help so participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the <a href="https://debconf21.debconf.org/about/coc/">web page about the Code of Conduct in DebConf21 website</a>
for more details on this.
</p>

<p>
Debian thanks the commitment of numerous <a href="https://debconf21.debconf.org/sponsors/">sponsors</a>
to support DebConf21, particularly our Platinum Sponsors:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://code4life.roche.com/">Roche</a>,
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>
and <a href="https://google.com/">Google</a>.
</p>

<h2>About Debian</h2>
<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>About DebConf</h2>

<p>
DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>About Lenovo</h2>
<p>
As a global technology leader manufacturing a wide portfolio of connected products,
including smartphones, tablets, PCs and workstations as well as AR/VR devices,
smart home/office and data center solutions, <a href="https://www.lenovo.com">Lenovo</a>
understands how critical open systems and platforms are to a connected world.
</p>

<h2>About Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 
</p>

<h2>About Roche</h2>
<p>
<a href="https://code4life.roche.com/">Roche</a> is a major international pharmaceutical provider and research company
dedicated to personalized healthcare. More than 100.000 employees worldwide
work towards solving some of the greatest challenges for humanity using
science and technology. Roche is strongly involved in publicly funded
collaborative research projects with other industrial and academic partners
and have supported DebConf since 2017.
</p>

<h2>About Amazon Web Services (AWS)</h2>
<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> is one of the world's
most comprehensive and broadly adopted cloud platform,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.
</p>

<h2>About Google</h2>
<p>
<a href="https://google.com/">Google</a> is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and hardware.
</p>
<p>
Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts 
of <a href="https://salsa.debian.org">Salsa</a>'s continuous integration infrastructure
within Google Cloud Platform.
</p>

<h2>Contact Information</h2>

<p>For further information, please visit the DebConf21 web page at
<a href="https://debconf21.debconf.org/">https://debconf21.debconf.org/</a>
or send mail to &lt;press@debian.org&gt;.</p>
