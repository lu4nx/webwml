<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were fixed in the reSIProcate SIP stack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11521">CVE-2017-11521</a>

    <p>The SdpContents::Session::Medium::parse function allowed remote
    attackers to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12584">CVE-2018-12584</a>

    <p>The ConnectionBase::preparseNewBytes function allowed remote
    attackers to cause a denial of service or possibly execute arbitrary
    code when TLS communication is enabled.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:1.11.0~beta1-3+deb9u2.</p>

<p>We recommend that you upgrade your resiprocate packages.</p>

<p>For the detailed security status of resiprocate please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/resiprocate">https://security-tracker.debian.org/tracker/resiprocate</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2865.data"
# $Id: $
