<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that jupyter-core, the base framework for Jupyter projects
like Jupyter Notebooks, could execute arbitrary code when loading
configuration files.</p>

<p>For Debian 10 buster, this problem has been fixed in version
4.4.0-2+deb10u1.</p>

<p>We recommend that you upgrade your jupyter-core packages.</p>

<p>For the detailed security status of jupyter-core please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jupyter-core">https://security-tracker.debian.org/tracker/jupyter-core</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3195.data"
# $Id: $
