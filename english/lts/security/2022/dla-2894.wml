<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>David Bouman discovered a heap-based buffer overflow vulnerability in
the base64 functions of aide, an advanced intrusion detection system,
which can be triggered via large extended file attributes or ACLs. This
may result in denial of service or privilege escalation.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.16-1+deb9u1.</p>

<p>We recommend that you upgrade your aide packages.</p>

<p>For the detailed security status of aide please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/aide">https://security-tracker.debian.org/tracker/aide</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2894.data"
# $Id: $
