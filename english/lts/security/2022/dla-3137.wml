<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Node.js, a JavaScript
runtime environment, which could result in memory corruption, invalid
certificate validation, prototype pollution or command injection.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22930">CVE-2021-22930</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-22940">CVE-2021-22940</a></p>

    <p>Use after free attack where an attacker might be able to exploit
    the memory corruption, to change process behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22939">CVE-2021-22939</a>

    <p>If the Node.js https API was used incorrectly and <q>undefined</q> was
    in passed for the <q>rejectUnauthorized</q> parameter, no error was
    returned and connections to servers with an expired certificate
    would have been accepted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21824">CVE-2022-21824</a>

    <p>Due to the formatting logic of the "console.table()" function it
    was not safe to allow user controlled input to be passed to the
    <q>properties</q> parameter while simultaneously passing a plain object
    with at least one property as the first parameter, which could be
    <q>__proto__</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32212">CVE-2022-32212</a>

    <p>OS Command Injection vulnerability due to an insufficient
    IsAllowedHost check that can easily be bypassed because
    IsIPAddress does not properly check if an IP address is invalid
    before making DBS requests allowing rebinding attacks.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
10.24.0~dfsg-1~deb10u2.</p>

<p>We recommend that you upgrade your nodejs packages.</p>

<p>For the detailed security status of nodejs please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nodejs">https://security-tracker.debian.org/tracker/nodejs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3137.data"
# $Id: $
