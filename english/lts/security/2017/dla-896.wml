<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Thunderbird mail
client: Multiple memory safety errors, buffer overflows and other
implementation errors may lead to the execution of arbitrary code or spoofing.</p>

<p>With version 45.8 Debian drops it's custom branding from the Icedove package
and ships the mail client as Thunderbird again. Please see the link below for
further information:
 <a href="https://wiki.debian.org/Thunderbird">https://wiki.debian.org/Thunderbird</a></p>

<p>Transition packages for the Icedove packages are provided which
automatically upgrade to the new version. Since new binary packages need
to be installed, make sure to allow that in your upgrade procedure (e.g.
by using "apt-get dist-upgrade" instead of "apt-get upgrade").</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:45.8.0-3~deb7u1.</p>

<p>We recommend that you upgrade your icedove packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-896.data"
# $Id: $
