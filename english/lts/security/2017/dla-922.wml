<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or have other
impacts.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2188">CVE-2016-2188</a>

    <p>Ralf Spenneberg of OpenSource Security reported that the iowarrior
    device driver did not sufficiently validate USB descriptors.  This
    allowed a physically present user with a specially designed USB
    device to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9604">CVE-2016-9604</a>

    <p>It was discovered that the keyring subsystem allowed a process to
    set a special internal keyring as its session keyring.  The
    security impact in this version of the kernel is unknown.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10200">CVE-2016-10200</a>

    <p>Baozeng Ding and Andrey Konovalov reported a race condition in the
    L2TP implementation which could corrupt its table of bound
    sockets.  A local user could use this to cause a denial of service
    (crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2647">CVE-2017-2647</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6951">CVE-2017-6951</a>

    <p>idl3r reported that the keyring subsystem would allow a process
    to search for <q>dead</q> keys, causing a null pointer dereference.
    A local user could use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2671">CVE-2017-2671</a>

    <p>Daniel Jiang discovered a race condition in the ping socket
    implementation.  A local user with access to ping sockets could
    use this to cause a denial of service (crash) or possibly for
    privilege escalation.  This feature is not accessible to any
    users by default.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5967">CVE-2017-5967</a>

    <p>Xing Gao reported that the /proc/timer_list file showed
    information about all processes, not considering PID namespaces.
    If timer debugging was enabled by a privileged user, this leaked
    information to processes contained in PID namespaces.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5970">CVE-2017-5970</a>

    <p>Andrey Konovalov discovered a denial-of-service flaw in the IPv4
    networking code. This can be triggered by a local or remote
    attacker if a local UDP or raw socket has the IP_RETOPTS option
    enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7184">CVE-2017-7184</a>

    <p>Chaitin Security Research Lab discovered that the net xfrm
    subsystem did not sufficiently validate replay state parameters,
    allowing a heap buffer overflow.  This can be used by a local user
    with the CAP_NET_ADMIN capability for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7261">CVE-2017-7261</a>

    <p>Vladis Dronov and Murray McAllister reported that the vmwgfx
    driver did not sufficiently validate rendering surface parameters.
    In a VMware guest, this can be used by a local user to cause a
    denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7273">CVE-2017-7273</a>

    <p>Benoit Camredon reported that the hid-cypress driver did not
    sufficiently validate HID reports.  This possibly allowed a
    physically present user with a specially designed USB device to
    cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7294">CVE-2017-7294</a>

    <p>Li Qiang reported that the vmwgfx driver did not sufficiently
    validate rendering surface parameters.  In a VMware guest, this
    can be used by a local user to cause a denial of service (crash)
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7308">CVE-2017-7308</a>

    <p>Andrey Konovalov reported that the packet socket (AF_PACKET)
    implementation did not sufficiently validate buffer parameters.
    This can be used by a local user with the CAP_NET_RAW capability
    for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7472">CVE-2017-7472</a>

    <p>Eric Biggers reported that the keyring subsystem allowed a thread
    to create new thread keyrings repeatedly, causing a memory leak.
    This can be used by a local user to cause a denial of service
    (memory exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7616">CVE-2017-7616</a>

    <p>Chris Salls reported an information leak in the 32-bit big-endian
    compatibility implementations of set_mempolicy() and mbind().
    This does not affect any architecture supported in Debian 7 LTS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7618">CVE-2017-7618</a>

    <p>Sabrina Dubroca reported that the cryptographic hash subsystem
    does not correctly handle submission of unaligned data to a
    device that is already busy, resulting in infinite recursion.
    On some systems this can be used by local users to cause a
    denial of service (crash).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.88-1.  This version also includes bug fixes from upstream version
3.2.88, and fixes some older security issues in the keyring, packet
socket and cryptographic hash subsystems that do not have CVE IDs.</p>

<p>For Debian 8 <q>Jessie</q>, most of these problems have been fixed in
version 3.16.43-1 which will be part of the next point release.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-922.data"
# $Id: $
