<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2816">CVE-2017-2816</a>

      <p>An exploitable buffer overflow vulnerability exists in the tag
      parsing functionality of LibOFX 0.9.11. A specially crafted OFX
      file can cause a write out of bounds resulting in a buffer
      overflow on the stack. An attacker can construct a malicious
      OFX file to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14731">CVE-2017-14731</a>

      <p>ofx_proc_file in ofx_preproc.cpp allows remote attackers to cause
      a denial of service (heap-based buffer over-read and application
      crash) via a crafted file</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.9.4-2.1+deb7u1.</p>

<p>We recommend that you upgrade your libofx packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1192.data"
# $Id: $
