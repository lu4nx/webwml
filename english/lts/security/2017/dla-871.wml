<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a TLS stripping vulnerability in the smptlib
library distributed with the CPython interpreter.</p>

<p>The library did not return an error if StartTLS failed, which might have
allowed man-in-the-middle attackers to bypass the TLS protections by leveraging
a network position to block the StartTLS command.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in python3.2 version
3.2.3-7+deb7u1.</p>

<p>We recommend that you upgrade your python3.2 packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-871.data"
# $Id: $
