<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in mutt, a sophisticated
text-based Mail User Agent, resulting in denial of service, stack-based
buffer overflow, arbitrary command execution, and directory traversal
flaws.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.5.23-3+deb8u1.</p>

<p>We recommend that you upgrade your mutt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1455.data"
# $Id: $
