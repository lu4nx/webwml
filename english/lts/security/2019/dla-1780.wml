<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Firefox 60.6.2 ESR repairs a certificate chain issue that caused
extensions to be disabled in the past few days.  More information, and
details of known remaining issues, can be found at
<a href="https://www.mozilla.org/firefox/60.6.2/releasenotes/">https://www.mozilla.org/firefox/60.6.2/releasenotes/</a> and
<a href="https://blog.mozilla.org/addons/2019/05/04/update-regarding-add-ons-in-firefox/">https://blog.mozilla.org/addons/2019/05/04/update-regarding-add-ons-in-firefox/</a></p>

<p>Installing this update will re-enable any extensions that were disabled
due to this issue.</p>

<p>Extensions installed from Debian packages were not affected.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
60.6.2esr-1~deb8u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1780.data"
# $Id: $
