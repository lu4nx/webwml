<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of libsdl2 released as DLA 1714-1 led to several regressions, as
reported by Avital Ostromich.  These regressions are caused by libsdl1.2
patches for <a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-7635">CVE-2019-7635</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-7638">CVE-2019-7638</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2019-7636">CVE-2019-7636</a> being
applied to libsdl2 without adaptations.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.0.2+dfsg1-6+deb8u2.</p>

<p>We recommend that you upgrade your libsdl2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1714-2.data"
# $Id: $
