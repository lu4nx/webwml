<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were found in the Apache HTTP server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

   <p>Matei <q>Mal</q> Badanoiu reported a limited cross-site scripting
   vulnerability in the mod_proxy error page.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

   <p>Yukitsugu Sasaki reported a potential open redirect vulnerability in
   the mod_rewrite module.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.10-10+deb8u15.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1900.data"
# $Id: $
