<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An update has been made to php5, a server-side, HTML-embedded scripting
language.  Specficially, as reported in #805222, the ability to build
extensions in certain older versions of PHP within Debian has been
hindered by an upstream change which first appeared in PHP 5.6.15.  This
update applies a fix which restores the ability to build PHP extensions
for Debian 8 <q>jessie</q> so that a forthcoming PECL extension update can be
built and released.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.6.40+dfsg-0+deb8u6.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1928.data"
# $Id: $
