<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were found in the rdesktop RDP client, which
could result in denial of service, information disclosure and the
execution of arbitrary code.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.8.4-0+deb8u1.</p>

<p>We recommend that you upgrade your rdesktop packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1683.data"
# $Id: $
