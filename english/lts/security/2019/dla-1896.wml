<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a remote arbitrary code
vulnerability in commons-beanutils, a set of utilities for
manipulating JavaBeans code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10086">CVE-2019-10086</a>

    <p>In Apache Commons Beanutils 1.9.2, a special BeanIntrospector class was added which allows suppressing the ability for an attacker to access the classloader via the class property available on all Java objects. We, however were not using this by default characteristic of the PropertyUtilsBean.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.9.2-1+deb8u1.</p>

<p>We recommend that you upgrade your commons-beanutils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1896.data"
# $Id: $
