<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13934">CVE-2020-13934</a>

    <p>An h2c direct connection to Apache Tomcat did not release the
    HTTP/1.1 processor after the upgrade to HTTP/2. If a sufficient
    number of such requests were made, an OutOfMemoryException could
    occur leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13935">CVE-2020-13935</a>

    <p>The payload length in a WebSocket frame was not correctly validated
    in Apache Tomcat. Invalid payload lengths could trigger an infinite
    loop. Multiple requests with invalid payload lengths could lead to a
    denial of service.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8.5.54-0+deb9u3.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>For the detailed security status of tomcat8 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2286.data"
# $Id: $
