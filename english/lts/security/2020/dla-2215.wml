<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVE(s) were found in src:clamav package.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3327">CVE-2020-3327</a>

    <p>A vulnerability in the ARJ archive parsing module in Clam
    AntiVirus (ClamAV) could allow an unauthenticated, remote
    attacker to cause a denial of service condition on an affected
    device. The vulnerability is due to a heap buffer overflow read.
    An attacker could exploit this vulnerability by sending a crafted
    ARJ file to an affected device. An exploit could allow the
    attacker to cause the ClamAV scanning process crash, resulting
    in a denial of service condition.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3341">CVE-2020-3341</a>

    <p>A vulnerability in the PDF archive parsing module in Clam
    AntiVirus (ClamAV) could allow an unauthenticated, remote
    attacker to cause a denial of service condition on an affected
    device. The vulnerability is due to a stack buffer overflow read.
    An attacker could exploit this vulnerability by sending a crafted
    PDF file to an affected device. An exploit could allow the
    attacker to cause the ClamAV scanning process crash, resulting
    in a denial of service condition.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.101.5+dfsg-0+deb8u2.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2215.data"
# $Id: $
