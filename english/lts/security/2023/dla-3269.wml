<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw in Apache libapreq2 versions 2.16 and earlier could cause a
buffer overflow while processing multipart form uploads. A remote
attacker could send a request causing a process crash which could lead
to a denial of service attack.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.13-7~deb10u2.</p>

<p>We recommend that you upgrade your libapreq2 packages.</p>

<p>For the detailed security status of libapreq2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapreq2">https://security-tracker.debian.org/tracker/libapreq2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3269.data"
# $Id: $
