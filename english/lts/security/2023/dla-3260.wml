<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that node-xmldom, a standard XML DOM (Level2 CORE)
implementation in pure javascript, processed ill-formed XML, which may
result in bugs and security holes in downstream applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21366">CVE-2021-21366</a>

    <p>xmldom versions 0.4.0 and older do not correctly preserve system
    identifiers, FPIs or namespaces when repeatedly parsing and serializing
    maliciously crafted documents.  This may lead to unexpected syntactic
    changes during XML processing in some downstream applications.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39353">CVE-2022-39353</a>

    <p>Mark Gollnick discovered that xmldom parses XML that is not well-formed
    because it contains multiple top level elements, and adds all root nodes to
    the <code>childNodes</code> collection of the <code>Document</code>, without reporting or throwing
    any error.  This breaks the assumption that there is only a single root node
    in the tree, and may open security holes such as <a href="https://security-tracker.debian.org/tracker/CVE-2022-39299">CVE-2022-39299</a> in
    downstream applications.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.1.27+ds-1+deb10u2.</p>

<p>We recommend that you upgrade your node-xmldom packages.</p>

<p>For the detailed security status of node-xmldom please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-xmldom">https://security-tracker.debian.org/tracker/node-xmldom</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3260.data"
# $Id: $
