msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "El sistema operativo universal"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "¡DebConf está en marcha!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "Logo de DebConf"

#: ../../english/index.def:19
msgid "DC19 Group Photo"
msgstr "Foto de grupo de DC19"

#: ../../english/index.def:22
msgid "DebConf19 Group Photo"
msgstr "Foto de grupo de DebConf19"

#: ../../english/index.def:26
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini DebConf Ratisbona 2021"

#: ../../english/index.def:29
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Foto de grupo de la MiniDebConf en Ratisbona 2021"

#: ../../english/index.def:33
msgid "Screenshot Calamares Installer"
msgstr "Captura de pantalla del instalador Calamares"

#: ../../english/index.def:36
msgid "Screenshot from the Calamares installer"
msgstr "Captura de pantalla del instalador Calamares"

#: ../../english/index.def:40 ../../english/index.def:43
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian es como una navaja suiza"

#: ../../english/index.def:47
msgid "People have fun with Debian"
msgstr "La gente se divierte con Debian"

#: ../../english/index.def:50
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Gente Debian pasándolo bien en DebConf18 en Hsinchu"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "«Bits from Debian», el blog de Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blog"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronoticias"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Micronoticias de Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planeta"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "El Planeta de Debian"
