#use wml::debian::translation-check translation="938f7bfa52fa35f75b35de70bbd10bfc81e42e8d"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el gestor de arranque GRUB2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14372">CVE-2020-14372</a>

    <p>Se descubrió que la orden acpi permite que un usuario con privilegios
    cargue tablas ACPI modificadas cuando está habilitado el arranque seguro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25632">CVE-2020-25632</a>

    <p>Se encontró una vulnerabilidad de «uso tras liberar» en la orden rmmod.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25647">CVE-2020-25647</a>

    <p>Se encontró una vulnerabilidad de escritura fuera de límites en la
    función grub_usb_device_initialize(), a la que se llama para inicializar
    dispositivos USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27749">CVE-2020-27749</a>

    <p>Se encontró un defecto de desbordamiento de pila en grub_parser_split_cmdline.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27779">CVE-2020-27779</a>

    <p>Se descubrió que la orden cutmem permite que un usuario con privilegios
    elimine regiones de memoria cuando está habilitado el arranque seguro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20225">CVE-2021-20225</a>

    <p>Se encontró una vulnerabilidad de escritura fuera de límites en la memoria dinámica («heap») en el analizador
    sintáctico de opciones en formato corto.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20233">CVE-2021-20233</a>

    <p>Se encontró un defecto de escritura fuera de límites en la memoria dinámica («heap») causada por errores al calcular
    el espacio requerido para el entrecomillado al representar los menús.</p></li>

</ul>

<p>Se puede encontrar información más detallada en
<a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot">https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot</a></p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.02+dfsg1-20+deb10u4.</p>

<p>Le recomendamos que actualice los paquetes de grub2.</p>

<p>Para información detallada sobre el estado de seguridad de grub2, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/grub2">https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4867.data"
