#use wml::debian::translation-check translation="1ee140ecea4124e2849e2bc5c2de4b63b838e5c5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans cURL, une bibliothèque
de transfert par URL :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8169">CVE-2020-8169</a>

<p>Marek Szlagor a signalé que libcurl pourrait être entraîné à préfixer le
nom d'hôte d'une partie du mot de passe avant sa résolution, divulguant
éventuellement le mot de passe partiel sur le réseau et vers le(s)
serveur(s) DNS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8177">CVE-2020-8177</a>

<p>sn a signalé que curl pourrait être entraîné par un serveur malveillant
à écraser un fichier local lors de l'utilisation des options -J
(--remote-header-name) et -i (--include) dans la même ligne de commande.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8231">CVE-2020-8231</a>

<p>Marc Aldorasi a signalé que libcurl pourrait utiliser la mauvaise
connexion quand une application utilisant plusieurs API de libcurl attribue
l'option CURLOPT_CONNECT_ONLY, ce qui pourrait conduire à des fuites
d'information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>

<p>Varnavas Papaioannou a signalé qu'un serveur malveillant pourrait
utiliser la réponse PASV pour entraîner curl à se reconnecter à une adresse
IP et à un port arbitraires, faisant éventuellement que curl extrait des
informations sur des services qui sont autrement privés et non divulgués.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8285">CVE-2020-8285</a>

<p>xnynx a signalé que libcurl pourrait épuiser l'espace de la pile lors de
l'utilisation de la fonctionnalité de correspondance de joker de FTP
(CURLOPT_CHUNK_BGN_FUNCTION).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8286">CVE-2020-8286</a>

<p>Il a été signalé que libcurl ne vérifiait pas si une réponse OCSP
correspondait effectivement au certificat attendu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22876">CVE-2021-22876</a>

<p>Viktor Szakats a signalé que libcurl ne retire pas les identifiants
d'utilisateur de l'URL lorsqu'il remplissait automatiquement le champ
d'en-tête Referer d'une requête HTTP dans les requêtes HTTP sortantes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22890">CVE-2021-22890</a>

<p>Mingtao Yang a signalé que, lors de l'utilisation d'un mandataire HTTPS
et de TLS 1.3, libcurl pourrait confondre les tickets de session provenant
du mandataire HTTPS comme s'ils provenaient plutôt du serveur distant. Cela
pourrait permettre à un mandataire HTTPS d'entraîner libcurl à utiliser le
mauvais ticket de session pour l'hôte et ainsi de contourner la
vérification du certificat TLS du serveur.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 7.64.0-4+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4881.data"
# $Id: $
