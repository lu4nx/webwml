#use wml::debian::translation-check translation="a0d25d78b2c207b5e643fbc99f4957ec35c99fda" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>L'utilisateur <q>Arminius</q> a découvert une vulnérabilité dans Vim,
une version améliorée de l'éditeur Vi standard d'UNIX (Vi IMproved). Le
<q>Common vulnérabilités and exposures project</q> identifie le problème
suivant :</p>

<p>Les éditeurs fournissent généralement un moyen pour intégrer des
commandes de configuration de l'éditeur (alias ligne de mode – « modeline »)
qui sont exécutées dès qu'un fichier est ouvert, tandis que les commandes
malfaisantes sont filtrées par un mécanisme de bac à sable. La commande
<q>source</q> (utilisée pour inclure et exécuter un autre fichier) n'était
pas filtrée, permettant l'exécution d'une commande shell avec un fichier
soigneusement contrefait ouvert dans Vim.</p>

<p>Pour la distribution stable (Stretch), ce problème a été corrigé dans la
version 2:8.0.0197-4+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vim, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4467.data"
# $Id: $
