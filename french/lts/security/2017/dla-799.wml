#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été découverts dans Ming. Ils pourraient
conduire à l'exécution de code arbitraire ou au plantage de l'application.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9264">CVE-2016-9264</a>

<p>dépassement de tampon global dans printMP3Headers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9265">CVE-2016-9265</a>

<p>division par zéro dans printMP3Headers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9266">CVE-2016-9266</a>

<p>décalage à gauche dans listmp3.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9827">CVE-2016-9827</a>

<p>listswf : dépassement de tampon basé sur le tas dans _iprintf</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9828">CVE-2016-9828</a>

<p>listswf : dépassement de tampon basé sur le tas dans _iprintf</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9829">CVE-2016-9829</a>

<p>listswf : déréférencement de pointeur NULL dans dumpBuffer</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9831">CVE-2016-9831</a>

<p>listswf : dépassement de tampon basé sur le tas dans parseSWF_RGBA</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.4.4-1.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ming.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-799.data"
# $Id: $
