#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités existaient dans la bibliothèque fournissant la sélection
et la matricialisation de fontes, libxfont.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13720">CVE-2017-13720</a>
<p>Si un modèle contenait un caractère « ? », n’importe quel caractère de la
chaîne était omis même s’il était un « \0 ». Le reste de la correspondance lisait
une mémoire non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13722">CVE-2017-13722</a>
<p>Un fichier PCF mal formé pouvait faire que la bibliothèque lise à partir d’une
mémoire de tas aléatoire qui était derrière le tampon de « strings »,
conduisant à un plantage d'application ou à une fuite d'informations.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 1:1.4.5-5+deb7u1 de libxfont.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxfont.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1126.data"
# $Id: $
