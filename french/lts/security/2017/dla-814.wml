#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7056">CVE-2016-7056</a>

<p>Une attaque temporelle locale a été découverte à l’encontre de ECDSA P-256.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8610">CVE-2016-8610</a>

<p>Il a été découvert qu’aucune limite n’était imposée pour des paquets d’alerte
lors de l’initiation de connexion SSL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3731">CVE-2017-3731</a>

<p>Robert Swiecki a découvert que le chiffrement RC4-MD5, lors d’une exécution
sur un système 32 bits, pourrait être forcé à une lecture hors limites,
aboutissant à un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.0.1t-1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-814.data"
# $Id: $
