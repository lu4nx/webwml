#use wml::debian::translation-check translation="8eb055b9e42d937c7bdbf44a8fb401266931a228" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans openjpeg2, une
bibliothèque d’image JPEG 2000.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9112">CVE-2016-9112</a>

<p>Une exception de virgule flottante ou une division par zéro dans la fonction
opj_pi_next_cprl pourraient conduire à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20847">CVE-2018-20847</a>

<p>Un calcul inexact de valeurs dans la fonction opj_get_encoding_parameters
peut conduire à un dépassement d'entier. Ce problème a été en partie réglé par
le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2015-1239">CVE-2015-1239</a>.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.1.0-2+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1851.data"
# $Id: $
