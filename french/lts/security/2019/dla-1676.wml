#use wml::debian::translation-check translation="e4928a7ce7d99be10f157a113e1f39a03cf01b60" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Ralph Dolmans et Karst Koymans ont trouvé un défaut dans la manière dont
unbound, un solveur de validation, mise en cache et récursif de DNS, validait
des enregistrements NSEC synthétisés avec des jokers.</p>

<p>Un enregistrement de joker NSEC improprement validé pourrait être utilisé
pour prouver la non-existence (réponse NXDOMAIN) d’un enregistrement de joker
existant, ou tromper unbound pour accepter une attestation NODATA.</p>

<p>Pour plus d’informations, veuillez vous référer à l’annonce amont sur
<url "https://unbound.net/downloads/CVE-2017-15105.txt">.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 1.4.22-3+deb8u4.

<p>Nous vous recommandons de mettre à jour vos paquets unbound.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1676.data"
# $Id: $
