#use wml::debian::translation-check translation="cee9bd81d1c23f03465625f11eb7aeb4754b0902" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le projet Drupal utilise la bibliothèque Archive_Tar de PEAR qui a publié
une mise à jour de sécurité qui impacte Drupal.</p>

<p>La vulnérabilité est mitigée par le fait que l’utilisation par le cœur de
Drupal de la bibliothèque Archive_Tar n’est pas vulnérable, car il ne permet
pas les liens symboliques.</p>

<p>L’exploitation peut être possible si du code tiers ou personnel utilise la
bibliothèque pour extraire les archives tar (par exemple, .tar, .tar.gz, .bz2
ou .tlz) provenant de sources non sûres.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 7.52-2+deb9u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets drupal7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de drupal7,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/drupal7">\
https://security-tracker.debian.org/tracker/drupal7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2721.data"
# $Id: $
