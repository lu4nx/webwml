#use wml::debian::translation-check translation="774651859c0541af8ce516952358a7ff0c2cfb60" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de traversée
de répertoires dans Django, un cadriciel populaire de développement web basé sur
Python.</p>

<p>Les classes MultiPartParser, UploadedFile et FieldFile permettaient une
traversée de répertoires à l’aide de fichiers téléversés avec des noms de
fichier spécialement élaborés. Pour mitiger ce risque, des noms de base plus
stricts et un nettoyage de chemin sont utilisés. En particulier, les noms de
fichier vides et les chemins avec des points sont refusés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31542">CVE-2021-31542</a></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.10.7-2+deb9u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2651.data"
# $Id: $
