#use wml::debian::translation-check translation="7feda280e03c75f0fc6b2380a750bce08b2f9d0d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Brève description</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3609">CVE-2021-3609</a>

<p>Norbert Slusarek a signalé une vulnérabilité de situation de compétition
dans la gestion de réseau de protocole CAN de BCM, permettant à un attaquant
local d’augmenter ses privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21781">CVE-2021-21781</a>

<p>« Lilith >_> » de Cisco Talos a découvert que le code d’initialisation
d’Arm n’initialise pas complètement le <q>sigpage</q> qui est mappé dans les
processus de l’espace utilisateur pour la gestion des signaux. Cela peut
aboutir à une fuite d’informations sensibles, particulièrement quand le
système est redémarré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33909">CVE-2021-33909</a>

<p>Qualys Research Labs a découvert une vulnérabilité de conversion
<q>size_t vers int</q> dans la couche système de fichiers du noyau Linux. Un
attaquant local non privilégié capable de créer, monter puis supprimer une
structure profonde de répertoires dont la longueur totale du chemin dépasse
1 Go, peut tirer avantage de ce défaut pour une élévation de privilèges.</p>

<p>Vous trouverez plus de détails dans l'annonce de Qualys à l'adresse
<a href="https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt">https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt</a></p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34693">CVE-2021-34693</a>

<p>Norbert Slusarek a signalé une fuite d’informations dans la gestion de
réseau de protocole CAN de BCM. Un attaquant local peut tirer avantage du
défaut pour obtenir des informations sensibles à partir de la mémoire de pile
du noyau.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.9.272-2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2713.data"
# $Id: $
