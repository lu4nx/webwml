#use wml::debian::translation-check translation="0489e1b952f47155cfd52286f4b52319011dbc06" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige plusieurs attaques distantes et locales par
déni de service et d'autres problèmes :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4387">CVE-2013-4387</a>

<p>ipv6 : les paquets udp suivant un paquet UFO mis en file d'attente
doivent aussi être gérés par UFO pour empêcher des attaquants distants de
provoquer un déni de service (corruption de mémoire et plantage du système)
ou éventuellement d’autres conséquences non précisées au moyen du trafic
réseau qui déclenche un gros paquet en réponse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4470">CVE-2013-4470</a>

<p>inet : correction d'une corruption de mémoire possible avec UDP_CORK et
UFO pour empêcher que des utilisateurs locaux provoquent un déni de service
(corruption de mémoire et plantage du système) ou éventuellement obtiennent
des privilèges à l'aide d'une application contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0203">CVE-2014-0203</a>

<p>correction de casse du point de montage magique autofs/afs/etc., évitant
des attaques par déni de service par des utilisateurs locaux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2678">CVE-2014-2678</a>

<p>rds : déréférencement d'un périphérique NULL évité dans
rds_iw_laddr_check pour empêcher des attaques locales par déni de service
(plantage du système ou éventuellement d’autres conséquences non précisées).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3122">CVE-2014-3122</a>

<p>Un verrouillage incorrect de la mémoire peut se traduire en un déni de
service local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3144">CVE-2014-3144</a>

/ <a href="https://security-tracker.debian.org/tracker/CVE-2014-3145">CVE-2014-3145</a>

<p>Un utilisateur local peut provoquer un déni de service (plantage du
système) grâce à des instructions BPF contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3917">CVE-2014-3917</a>

<p>auditsc : les accès au masque d'audit_krule nécessitent des vérifications
des liens pour éviter une attaque locale par déni de service (oops) ou
éventuellement la fuite de valeurs sensibles d'un seul bit provenant de la
mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4652">CVE-2014-4652</a>

<p>ALSA : control : protection des contrôles de l'utilisateur contre des
accès concurrents ayant pour conséquence une situation de compétition,
permettant éventuellement à des utilisateurs locaux d'accéder à des
informations sensibles provenant de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4656">CVE-2014-4656</a>

<p>ALSA : control : assurance que id->index ne déborde pas pour éviter un
déni de service à l'encontre du système de son par des utilisateurs locaux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4667">CVE-2014-4667</a>

<p>sctp : correction d'un problème de bouclage de sk_ack_backlog,
évitant un déni de service (interruption de socket) à l'aide d'un paquet
SCTP contrefait par des attaquants distants.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4699">CVE-2014-4699</a>

<p>Andy Lutomirski a découvert que l'appel système ptrace ne vérifiait pas
la validité du registre RIP dans l'API ptrace sur les processeurs x86_64.
Un utilisateur non privilégié pourrait utiliser ce défaut pour planter le
noyau (résultant en un déni de service) ou pour une élévation de privilèges.</p></li>

</ul>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 2.6.32-48squeeze8 de linux-2.6.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0015.data"
# $Id: $
