#use wml::debian::translation-check translation="66bf90f8fc76700f0dc4c61f3c55a800798ab361" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’une vulnérabilité dans php5, un langage de script
embarqué dans du HTML et côté serveur, pourrait conduire à un épuisement
de l’espace disque sur le serveur. La limite mémoire pourrait être atteinte
lors de l’utilisation de noms de fichier ou de champ excessivement longs,
aboutissant à l’arrêt du téléversement sans nettoyage postérieur.</p>

<p>En outre, la version embarquée de <q>file</q> est vulnérable au
<a href="https://security-tracker.debian.org/tracker/CVE-2019-18218">CVE-2019-18218</a>.
Comme cela ne peut pas être exploité de la même façon dans php5 et dans file, ce
problème n’est pas géré dans son propre CVE mais comme un bogue qui est corrigé
ici (restriction du nombre d’éléments CDF_VECTOR pour empêcher un
dépassement de tampon de tas (écriture hors limites de quatre octets)).</p>


<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 5.6.40+dfsg-0+deb8u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2261.data"
# $Id: $
