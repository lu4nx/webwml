#use wml::debian::translation-check translation="ea96b4b16423e107a8e4da21fbde3a4edfd70ae4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans <tt>openexr</tt>, un ensemble d’outils
pour manipuler des fichiers d’image OpenEXR, souvent utilisés dans l’industrie
de graphisme numérique pour des effets visuels et des animations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16588">CVE-2020-16588</a>

<p>Un problème de déréférencement de pointeur NULL existe dans OpenEXR 2.3.0
d’<i>Academy Software Foundation</i> dans generatePreview dans makePreview.cpp qui peut
provoquer un déni de service à l'aide d'un fichier EXR contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16589">CVE-2020-16589</a>

<p>Un dépassement de tampon de tas existe dans OpenEXR 2.3.0 d’<i>Academy
Software Foundation</i> dans writeTileData dans ImfTiledOutputFile.cpp qui peut
provoquer un déni de service à l'aide d'un fichier EXR contrefait.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.2.0-11+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openexr.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2491.data"
# $Id: $
