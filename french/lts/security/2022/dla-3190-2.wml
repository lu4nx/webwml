#use wml::debian::translation-check translation="53e33068587aa415c5576a97b95fa341171a5922" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Zhang Boyang a signalé que la mise à jour de grub2 publiée dans l’annonce
DLA 3190-1 ne corrigeait pas correctement les
<a href="https://security-tracker.debian.org/tracker/CVE-2022-2601">CVE-2022-2601</a> et <a href="https://security-tracker.debian.org/tracker/CVE-2022-3775">CVE-2022-3775</a>.
De nouveaux paquets mis à jour sont maintenant disponibles pour corriger ce
problème. Pour rappel, voici le texte originel de cette annonce.</p>

<p>Plusieurs problèmes ont été découverts dans le code de traitement des
fontes de GRUB2 qui pouvaient avoir pour conséquences des plantages et
éventuellement l'exécution de code arbitraire. Cela pouvait conduire à un
contournement d'UEFI Secure Boot sur les systèmes affectés.

<p>En outre, des problèmes ont été découverts dans le chargement de l'image
qui pouvaient éventuellement mener à des dépassements de mémoire.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.06-3~deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets grub2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de grub2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3190-2.data"
# $Id: $
