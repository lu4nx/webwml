#use wml::debian::translation-check translation="a954522ed54dd0f99d67c3b88ea2cfe26979e0d6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans OpenEXR, des
outils en ligne de commande et une bibliothèque pour le format d'image OpenEXR.
Des dépassements de tampon ou des lectures hors limites pouvaient conduire à un
déni de service (plantage d'application) lors du traitement d'un fichier image
mal formé.</p>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 2.2.1-4.1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openexr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openexr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/openexr">\
https://security-tracker.debian.org/tracker/openexr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3236.data"
# $Id: $
