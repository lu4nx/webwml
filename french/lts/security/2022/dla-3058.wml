#use wml::debian::translation-check translation="a40416a65147bf5538cfc9a7dd81f6e9522820f0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans libsndfile, une bibliothèque de
lecture et écriture de fichiers audio.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12562">CVE-2017-12562</a>

<p>Du fait d'une possible attaque de dépassement de tas, un attaquant
pouvait provoquer une attaque à distance par déni de service en forçant la
fonction à produire une grande quantité de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4156">CVE-2021-4156</a>

<p>En utilisant un fichier FLAC contrefait, un attaquant pouvait déclencher
une lecture hors limites qui pouvait probablement provoquer un plantage,
mais qui pouvait éventuellement divulguer des informations de la mémoire.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.0.27-3+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsndfile.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libsndfile, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libsndfile">\
https://security-tracker.debian.org/tracker/libsndfile</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3058.data"
# $Id: $
