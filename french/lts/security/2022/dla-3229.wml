#use wml::debian::translation-check translation="656444a360d72e549e239437ec3ccf95b72bbce6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans node-log4js, un portage de log4js dans Node.js, les permissions par
défaut de fichier pour des fichiers de journal créés par les modificateurs
(appenders) du fichier, de fileSync et de dateFile sont universellement
lisibles. Cela pourrait causer des problèmes si des fichiers de journal
contiennent des informations sensibles. Cela affecterait n’importe quel
utilisateur n’ayant pas fourni ses propres permissions pour des fichiers
à l’aide du paramètre de mode dans la configuration.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 4.0.2-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-log4js.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-log4js,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-log4js">\
https://security-tracker.debian.org/tracker/node-log4js</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3229.data"
# $Id: $
