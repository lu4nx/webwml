#use wml::debian::translation-check translation="9e6f429ad124c125c542fc0f5c379559bd4520ec" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>KiCad est une suite de programmes pour la création de circuits imprimés.
Il inclut un éditeur de schémas, un outil de conception de PCB, des outils
d'assistance et un afficheur 3D pour visualiser le PCB finalisé avec ses
composants. </p>

<p>Plusieurs dépassements de tampons ont été découverts dans l'afficheur
Gerber et dans l'analyseur de fichier excellon, qui pouvaient conduire à
l'exécution de code lors de l'ouverture d'un fichier contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23803">CVE-2022-23803</a>

<p>Une vulnérabilité de dépassement de pile existe dans la fonction
d'analyse de coordonnées ReadXYCoord de fichiers gerber et excellon de
l'afficheur Gerber dans KiCad EDA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23804">CVE-2022-23804</a>

<p>Une vulnérabilité de dépassement de pile existe dans la fonction
d'analyse de coordonnées ReadIJCoord de fichiers gerber et excellon de
l'afficheur Gerber dans KiCad EDA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23946">CVE-2022-23946</a>

<p>Une vulnérabilité de dépassement de pile existe dans la fonction
d'analyse de coordonnées GCodeNumber de fichiers gerber et excellon de
l'afficheur dans KiCad EDA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23947">CVE-2022-23947</a>

<p>Une vulnérabilité de dépassement de pile existe dans la fonction
d'analyse de coordonnées DCodeNumber de fichiers gerber et excellon de
l'afficheur dans KiCad EDA.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.0.5+dfsg1-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets kicad.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de kicad, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/kicad">\
https://security-tracker.debian.org/tracker/kicad</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2998.data"
# $Id: $
