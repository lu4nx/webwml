#use wml::debian::translation-check translation="0e74f2775c135bae7777e4bfd9fb6fb606973d8b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>menglong2234 a découvert une exception de pointeur NULL dans net-snmp, une
suite d’applications SNMP (Simple Network Protocole Management), qui pouvait
aboutir à un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44792">CVE-2022-44792</a>

<p>Un attaquant distant (avec droit d’écriture) pouvait déclencher un
déréférencement de pointeur NULL lors du traitement ipDefaultTTL à l'aide d'un
paquet UDP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44793">CVE-2022-44793</a>

<p>Un attaquant distant (avec droit d’écriture) pouvait déclencher un
déréférencement de pointeur NULL lors du traitement ipv6IpForwarding à l'aide
d'un paquet UDP contrefait.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.7.3+dfsg-5+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets net-snmp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de net-snmp,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/net-snmp">\
https://security-tracker.debian.org/tracker/net-snmp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3270.data"
# $Id: $
