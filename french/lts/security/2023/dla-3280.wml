#use wml::debian::translation-check translation="be762402fb4af6e2b225f5edd997979f9f6f2b9a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans libde265, une implémentation au
code source ouvert du codec vidéo H.265, qui pouvaient aboutir à un déni de
service ou à un autre impact non précisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21596">CVE-2020-21596</a>

<p>libde265 version 1.0.4 contenait un dépassement de tampon global dans la
fonction decode_CABAC_bit, qui pouvait être exploité à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21597">CVE-2020-21597</a>

<p>libde265 version 1.0.4 contenait un dépassement de tampon de tas dans la
fonction mc_chroma, qui pouvait être exploité à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21598">CVE-2020-21598</a>

<p>libde265 version 1.0.4 contenait un dépassement de tampon de tas dans la
fonction ff_hevc_put_unweighted_pred_8_sse, qui pouvait être exploité à l'aide
d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43235">CVE-2022-43235</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de ff_hevc_put_hevc_epel_pixels_8_sse dans
sse-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un
déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43236">CVE-2022-43236</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de pile à l’aide de put_qpel_fallback&lt;unsigned short&gt;
dans fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de
provoquer un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43237">CVE-2022-43237</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de pile à l’aide de void put_epel_hv_fallback&lt;unsigned
short&gt; dans fallback-motion.cc. Cette vulnérabilité permettait à des attaquants
de provoquer un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43238">CVE-2022-43238</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait un plantage inconnu
à l’aide de ff_hevc_put_hevc_qpel_h_3_v_3_sse dans sse-motion.cc. Cette
vulnérabilité permettait à des attaquants de provoquer un déni de service
(DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43239">CVE-2022-43239</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de mc_chrom&lt;unsigned short&gt; dans
motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un déni
de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43240">CVE-2022-43240</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de ff_hevc_put_hevc_qpel_h_2_v_1_sse dans
sse-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un
déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43241">CVE-2022-43241</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait un plantage inconnu à
l’aide de ff_hevc_put_hevc_qpel_v_3_8_sse dans sse-motion.cc. Cette
vulnérabilité permettait à des attaquants de provoquer un déni de service
(DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43242">CVE-2022-43242</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de mc_lum&lt;unsigned char&gt; dans
motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un déni
de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43243">CVE-2022-43243</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de ff_hevc_put_weighted_pred_avg_8_sse
dans sse-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer
un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43244">CVE-2022-43244</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de put_qpel_fallback&lt;unsigned short&gt;
dans fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de
provoquer un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43245">CVE-2022-43245</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une erreur de
segmentation à l'aide de apply_sao_internal&lt;unsigned short&gt; dans sao.cc.
Cette vulnérabilité permettait à des attaquants de provoquer un déni de service
(DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43248">CVE-2022-43248</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de put_weighted_pred_avg_16_fallback dans
fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer
un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43249">CVE-2022-43249</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de put_epel_hv_fallback&lt;unsigned short&gt;
dans fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de
provoquer un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43250">CVE-2022-43250</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de put_qpel_0_0_fallback_16 dans
fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer
un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43252">CVE-2022-43252</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de put_epel_16_fallback dans
fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer
un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43253">CVE-2022-43253</a>

<p>Il a été découvert que libde265 version 1.0.8 contenait une vulnérabilité de
dépassement de tampon de tas à l’aide de put_unweighted_pred_16_fallback dans
fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer
un déni de service (DoS) à l'aide d'un fichier vidéo contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47655">CVE-2022-47655</a>

<p>libde265 version 1.0.9 est vulnérable à un dépassement de tampon dans la
fonction void put_qpel_fallback&lt;unsigned short&gt;.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.0.3-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libde265.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libde265,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libde265">\
https://security-tracker.debian.org/tracker/libde265</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3280.data"
# $Id: $
