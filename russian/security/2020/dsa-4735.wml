#use wml::debian::translation-check translation="f316e0ce25840c6590881cb5b3cec62cc137c07d" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>Несколько уязвимостей было обнаружено в загрузчике GRUB2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10713">CVE-2020-10713</a>

    <p>В коде для грамматического разбора grub.cfg была обнаружена уязвимость, позволяющая
    сломать UEFI Secure Boot и загрузить произвольный код. Подробности можно найти по адресу
    <a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">\
    https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14308">CVE-2020-14308</a>

    <p>Было обнаружено, что функция grub_malloc не выполняет проверку размера выделенного
    буфера памяти, что позволяет вызывать арифметическое переполнение и последующее
    переполнение буфера.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14309">CVE-2020-14309</a>

    <p>Переполнение целых чисел в функции grub_squash_read_symlink может приводить к переполнению буфера.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14310">CVE-2020-14310</a>

    <p>Переполнение целых чисел в функции read_section_from_string может приводить к переполнению буфера.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14311">CVE-2020-14311</a>

    <p>Переполнение целых чисел в функции grub_ext2_read_link может приводить к
    переполнению буфера.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15706">CVE-2020-15706</a>

    <p>script: предотвращение использования указателей после освобождения памяти при переопределении функции
    в ходе выполнения.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15707">CVE-2020-15707</a>

    <p>Переполнение целых чисел в коде для обработки размера initrd.</p></li>
</ul>

<p>Дополнительную информацию можно найти по адресу
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">\
https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot</a></p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2.02+dfsg1-20+deb10u1.</p>

<p>Рекомендуется обновить пакеты grub2.</p>

<p>С подробным статусом поддержки безопасности grub2 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4735.data"
