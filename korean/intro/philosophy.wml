#use wml::debian::template title="데비안 철학: 왜 하는가 그리고 어떻게 하는가" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Sebul" 

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">우리 사명: 자유 운영체제 만들기</a></li>
    <li><a href="#how">우리 가치: 데비안 커뮤니티가 어떻게 일하는가</a></li>
  </ul>
</div>

<h2><a id="freedom">우리 사명: 자유 운영체제 만들기</a></h2>

<p>데비안 프로젝트는 공동의 목표를 공유하는 개인들의 연합체입니다: 
우리는 누구나 자유롭게 사용할 수 있는 무료 운영 체제를 만들고 싶습니다.
이제 "free"라는 단어를 사용할 때 우리는 돈을 말하는 것이 아니라, 소프트웨어의 <em>freedom</em>을 말합니다.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">자유 소프트웨어 정의</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">자유 소프트웨어 재단 성명서</a></button></p>

<p>아마도 많은 사람들이 비용을 청구하지 않고 모든 것을 제공하기 위해 소프트웨어를 작성하고 
신중하게 패키징하고 유지 관리하는 데 많은 시간을 쓰는 까닭이 궁금할 것입니다. 
글쎄요, 많은 이유가 있습니다. 그 중 일부:</p>

<ul>
  <li>어떤 사람들은 단순히 다른 사람들을 돕고 싶어하며, 자유 소프트웨어 프로젝트에 기여하는 것은 이것을 달성하는 좋은 방법입니다.</li>
  <li>많은 개발자는 컴퓨터, 다양한 아키텍처 및 프로그래밍 언어에 대해 더 많이 배우기 위해 프로그램을 작성합니다.</li>
  <li>일부 개발자는 다른 사람들로부터 받은 모든 훌륭한 자유 소프트웨어에 대해 "고맙습니다"라고 말하기 위해 기여합니다.</li>
  <li>학계의 많은 사람들이 연구 결과를 공유하기 위해 자유 소프트웨어를 만듭니다.</li>
  <li>회사는 자유 소프트웨어를 유지하는 것을 돕기도 합니다: 애플리케이션 개발 방식에 영향을 미치거나 새로운 기능을 빠르게 구현하기 위해.</li>
  <li>물론 대부분의 데비안 개발자들이 참여합니다. 재미있다고 생각하기 때문!</li>
</ul>

<p>우리는 자유 소프트웨어를 믿지만 - 원하든 원하지 않든 - 사람들이 때때로 기계에 비자유 소프트웨어를 설치해야 한다는 점을 존중합니다. 
우리는 가능한 한 이러한 사용자를 지원하기로 결정했습니다. 
데비안 시스템에 비자유 소프트웨어를 설치하는 패키지가 점점 늘어나고 있습니다.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 우리는 자유 소프트웨어에 전념하고 있으며 그 약속을 문서로 공식화했습니다: 
우리 <a href="$(HOME)/social_contract">사회 계약</a></p>
</aside>

<h2><a id="how">우리 가치: 데비안 커뮤니티가 어떻게 일하는가</a></h2>

<p>데비안 프로젝트는 1000명 넘는 활동적인 <a
href="people">개발자 및 기여자</a>가 <a
href="$(DEVEL)/developers.loc">세상에</a> 퍼져있습니다. 
이런 큰 프로젝트는 조심스럽게 <a href="organization">조직된 구조</a>가 필요합니다.
따라서 데비안 프로젝트가 어떻게 일하는지, 데비안 커뮤니티에 규칙과 지침이 있는지 궁금하다면 다음 설명을 보세요:</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">데비안 헌법</a>: <br>
          이 문서는 조직 구조를 설명하고 데비안 프로젝트가 공식적인 결정을 내리는 방법을 설명합니다.</li>
        <li><a href="../social_contract">사회 계약 및 자유 소프트웨어 지침</a>: <br>
        Debian Social Contract 및 Debian Free Software Guidelines (DFSG) 자유 소프트웨어와 자유 소프트웨어 커뮤니티에 대한 우리의 약속을 설명합니다.</li>
        <li><a href="diversity">다양성 선언:</a> <br>
        데비안 프로젝트는 여러분이 자신을 어떻게 식별하든, 다른 사람들이 여러분을 어떻게 인식하든, 상관없이 모든 사람이 참여하는 것을 환영하고 권장합니다.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">행동강령:</a> <br>
        우리는 메일링 리스트, IRC 채널 등의 참가자를 위한 행동강령을 채택했습니다.</li>
        <li><a href="../doc/developers-reference/">개발자 참조:</a> <br>
        이 문서는 데비안 개발자와 유지 관리자에게 권장되는 절차와 사용 가능한 리소스에 대한 개요를 제공합니다.</li>
        <li><a href="../doc/debian-policy/">데비안 정책:</a> <br>
        데비안 배포에 대한 정책 요구 사항을 설명하는 설명서. 예를 들어 데비안 아카이브의 구조와 내용, 모든 패키지가 포함되기 위해 충족해야 하는 기술적 요구 사항 등</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> 데비안 내부: <a href="$(DEVEL)/">개발자 코너</a></button></p>
