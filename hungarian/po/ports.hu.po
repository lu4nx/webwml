msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:47+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian Alpha-ra"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian PA-RISC-re"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "Hurd CD-k"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian IA-64-re"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Kapocslat"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "CPU-k"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Kreditek"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Fejlesztés"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Dokumentáció"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Telepítés"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr ""

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Linkek"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Hírek"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Portolás"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Portok"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Problémák"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Szoftver térkép"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Állapot"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Ellátás"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Rendszerek"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD i386-ra"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD Alpha-ra"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Miért?"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Emberek"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian PowerPC-re"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian Sparc-ra"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian Sparc64-re"

#~ msgid "Debian for S/390"
#~ msgstr "Debian S/390-re"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian MIPS-re"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian Motorola 680x0-ra"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debain GNU/FreeBSD"

#~ msgid "Main"
#~ msgstr "Fő"

#~ msgid "Debian for Beowulf"
#~ msgstr "Bebian Beowolf-ra"

#~ msgid "Debian for ARM"
#~ msgstr "Debian ARM-ra"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian AMD64-re"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian laptopokra"
