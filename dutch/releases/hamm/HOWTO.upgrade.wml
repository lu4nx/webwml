#use wml::debian::template title="Opwaarderen naar Debian 2.0 op x86-machines"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd"

<P>Om problemen te voorkomen bij het opwaarderen van pakketten via dpkg, dselect
of dftp (vanwege mogelijke conflicten tussen libc5 en libc6), wordt een speciale
opwaarderingsprocedure aanbevolen. Dit document beschrijft die procedure.

<P>Er zijn verschillende manieren om op te waarderen vanaf een eerdere versie:
  <ol>
  <li>autoup.sh<br>
       Dit is een script dat de programma's in de juiste volgorde opwaardeert
      en zelfs de deb's voor u downloadt. Vanwege de voortdurende wijzigingen
      aan het archief, wordt een tar-archief geleverd van de pakketten die
      beschikbaar waren op het moment dat autoup.sh voor het laatst werd
      uitgebracht. Dit is beschikbaar op de volgende sites:
        <ul>
        <li><a href="https://www.debian.org/releases/hamm/autoup/">https://www.debian.org/releases/hamm/autoup/</a></li>
        <li><a href="http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/">http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/</a></li>
        </ul>
  <li>apt-get<br>
       Dit is het commandoregelgedeelte van het toekomstige programma voor
      pakketbeheer van Debian. Het is in staat de pakketten juist te ordenen en
      zal ze downloaden van een lokaal archief en van http- en ftp-sites. Het
      kan de informatie van verschillende sites samenvoegen, zodat u een cd, een
      up-to-date spiegelserver en een niet-Amerikaanse site kunt gebruiken voor
      de beste mix van snelheid, variatie en de nieuwste versies.
      Geef gewoon de opdrachten 'apt-get update; apt-get dist-upgrade'.
      De versie van 'Bo' is te vinden op:
     <a href="http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/">http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/</a>.
      De versie van apt-get in Bo is goed getest. apt-get maakt officieel deel
      uit van het verpakkingssysteem vanaf slink.
  <li>Handmatig<br>
       Er is een HOWTO te vinden op:
     <a href="$HOME/releases/hamm/autoup/libc5-libc6-Mini-HOWTO.html">
     $HOME/releases/hamm/autoup/libc5-libc6-Mini-HOWTO.html</a>,
     maar aangezien het script autoup.sh dit proces gewoon automatiseert, is
     dit de minst handige keuzemogelijkheid.
  </ol>

<H3>Vragen en antwoorden</H3>
<pre>
V: Waarom niet gewoon zoals normaal de ftp-methode van dselect gebruiken?
A: Die kan de pakketinstallatie niet correct ordenen, zodat geen vloeiende
   opwaardering gegarandeerd kan worden. Er wordt gewerkt aan APT, een nieuw
   front-end voor dpkg. Met apt zullen alle toekomstige opwaarderingen
   eenvoudiger zijn dan ooit en zullen ze niet lijden onder dit soort ongemakken.

V: Oké, Ik voerde de opwaardering uit, kan ik weer veilig dingen doen?
A: Ja, u kunt dselect opnieuw gebruiken.
</pre>

