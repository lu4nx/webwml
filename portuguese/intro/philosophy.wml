#use wml::debian::template title="Nossa filosofia: Por que fazemos e como fazemos" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

# tradutores(as): parte do texto foi retirado de /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Nossa missão: criar um sistema operacional livre</a></li>
    <li><a href="#how">Nossos valores: como a comunidade Debian funciona</a></li>
  </ul>
</div>

<h2><a id="freedom">Nossa missão: criar um sistema operacional livre</a></h2>

<p>O projeto Debian é uma associação de pessoas que compartilha um objetivo em
comum: queremos criar um sistema operacional livre, disponível livremente
para todas as pessoas. Agora, quando usamos a palavra "livre", não estamos
falando de dinheiro, mas sim de <em>liberdade</em> do software.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">Nossa definição de Software Livre</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">Leia a declaração da Free Software Foundation</a></button></p>

<p>Talvez você esteja se perguntando por que tantas pessoas optam por gastar
muitas horas do seu próprio tempo escrevendo software, cuidadosamente
empacotando e mantendo os pacotes, apenas para doar tudo sem cobrar por isso?
Bem, há muitos motivos, aqui estão alguns deles:</p>

<ul>
  <li>Algumas pessoas simplesmente gostam de ajudar outras, e contribuir para um
projeto de software livre é uma ótima maneira de conseguir isso.</li>
   <li>Muitos(as) desenvolvedores(as) escrevem programas para aprender mais
sobre computadores, diferentes arquiteturas e linguagens de programação.</li>
   <li>Alguns(mas) desenvolvedores(as) contribuem para dizer "obrigado(a)" por
todo o excelente software livre que receberam de outros(as).</li>
   <li>Muitas pessoas na academia criam software livre para compartilhar os
resultados de suas pesquisas. </li>
   <li>As empresas também ajudam a manter o software livre: para influenciar
como os aplicativos se desenvolvem ou para implementar novos recursos rapidamente. </li>
   <li>Claro, a maioria dos(as) desenvolvedores(as) Debian participa porque
eles(as) acham que é muito divertido! </li>
</ul>

<p>Embora acreditemos no software livre, respeitamos que as pessoas às vezes
tenham que instalar software não livre em suas máquinas – queiram elas ou não.
Decidimos apoiar esses(as) usuários(as), sempre que possível. Há um número
crescente de pacotes que instalam software não livre em um sistema Debian.</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Estamos comprometidos com o
Software Livre e formalizamos esse compromisso em um documento: nosso <a href="$(HOME)/social_contract">contrato social</a></p>
</aside>

<h2><a id="how">Nossos valores: como a comunidade Debian funciona</a></h2>

<p>O projeto Debian tem mais de mil
<a href="people">desenvolvedores(as) e contribuidores(as)</a> ativos(as)
espalhados(as) <a href="$(DEVEL)/developers.loc">por todo o mundo</a>.
Um projeto desse tamanho precisa de uma
<a href="organization">estrutura organizada</a>.
Então, se você quer saber como o projeto Debian funciona e se a comunidade
Debian tem regras e diretrizes, dê uma olhada nas seguinte declarações:

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">A constituição Debian</a>: <br>
          Este documento descreve a estrutura organizacional e explica como o
          projeto Debian toma decisões formais.</li>
        <li><a href="../social_contract">O contrato social e a definição Debian de Software Livre</a>: <br>
          O contrato social do Debian e as definição Debian de Software Livre
          (DFSG - Debian Free Software Guidelines) como parte deste contrato,
          descrevem nosso compromisso com o Software Livre e com a comunidade de
          Software Livre.</li>
        <li><a href="diversity">A declaração de diversidade</a> <br>
          O projeto Debian dá boas-vindas e encoraja todas as pessoas a
          participarem, não importa como você se identifique ou como os outros
          o(a) vejam.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">O código de conduta:</a> <br>
          Adotamos um código de conduta para os(as) participantes em nossas
          listas de discussão, canais de IRC, etc.</li>
        <li><a href="../doc/developers-reference/">A referência do(a) desenvolvedor(a):</a> <br>
          Este documento fornece uma visão geral dos procedimentos recomendados
          e dos recursos disponíveis para desenvolvedores(as) e mantenedores(as)
          Debian.</li>
        <li><a href="../doc/debian-policy/">A política Debian:</a> <br>
          Um manual que descreve os requisitos da política para a distribuição
          Debian, por exemplo a estrutura e os conteúdos do repositório Debian,
          os requisitos técnicos que cada pacote deve satisfazer para ser
          incluído, etc.</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span>Por dentro do Debian: <a href="$(DEVEL)/">Canto dos(as) desenvolvedores(as)</a></button></p>

